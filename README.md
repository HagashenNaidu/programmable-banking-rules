# Programmable Banking Rules

A Blazor WebAssembly app that uses a Warewolf back-end to allow the customization of rules for accepting/declining transactions using Investec's Programmable Banking platform.

[SLIDE DECK](https://docs.google.com/presentation/d/e/2PACX-1vSFnXlFHLZoc4RbMmZ3VUZBMf4wVC1R9gvzrvSbYwaxu9reHXSrQ5URuz9VEujEmVV64x9hDPwSKT0h/pub?start=false&loop=false&delayms=3000)